1. Clone this project
```bash
git clone https://gitlab.com/TengHsiang/systrace.git
```

2. Porting code src/tracer.cpp and include/tracer.h into your program

3. Run systrace with python
```bash
python systrace.py --target=linux
```

4. Run your program

5. Open trace.html